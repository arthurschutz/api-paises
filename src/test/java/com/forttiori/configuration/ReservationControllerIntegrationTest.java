package com.forttiori.configuration;

import com.forttiori.configuration.controller.ReservationController;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import com.forttiori.configuration.service.reservations.ReservationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ReservationController.class, ReservationServiceImpl.class})
class ReservationControllerIntegrationTest {

    @MockBean
    ReservationRepository reservationRepository;

    @Autowired
    ReservationServiceImpl reservationService;

    @Test
    void deveSalvarReserva() {
        when(reservationRepository.save(MapperService.mapToReservationEntity(serviceRequestDTO))).thenReturn(reservation);

        var atual = this.reservationService.save(serviceRequestDTO);
        var stubExpected = ServiceResponseDTO.builder()
                .id(serviceResponseDTO.getId())
                .name(serviceResponseDTO.getName())
                .capital(serviceResponseDTO.getCapital())
                .region(serviceResponseDTO.getRegion())
                .build();

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveFindAllReservas() {
        when(reservationRepository.findAll()).thenReturn(reservationList);

        var atual = reservationService.findAll();
        var stubExpected = MapperService.mapListReservationToListResponse(reservationList);

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveFindReservaByID() {
        when(reservationRepository.findById("1")).thenReturn(Optional.ofNullable(reservation));

        var atual = reservationService.findByIDResponse("1");
        var stubExpected = serviceResponseDTO;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveAtualizarReserva() {
        when(reservationRepository.existsById("1")).thenReturn(true);
        when(reservationRepository.save(reservation)).thenReturn(reservation);

        var atual = reservationService.update("1", serviceRequestDTO);
        var stubExpected = serviceResponseDTO;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveDeletarUmaReserva() {
        when(reservationRepository.existsById("1")).thenReturn(true);
        reservationService.delete("1");
        verify(reservationRepository, times(1)).deleteById("1");
    }

    @Test
    void deveDeleteManyReservas() {
        List<Reservation> reservas = reservationList;
        List<String> ids = Arrays.asList("1", "2", "3");

        when(reservationRepository.findAllById(ids)).thenReturn(reservas);

        var atual = reservationService.deleteMany(ids);
        var stubExpected = reservas;

        assertEquals(stubExpected, atual);
    }

    ServiceRequestDTO serviceRequestDTO = ServiceRequestDTO.builder()
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    ServiceResponseDTO serviceResponseDTO = ServiceResponseDTO.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    Reservation reservation = Reservation.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    List<Reservation> reservationList = Arrays.asList(
            Reservation.builder().id("1").capital("Lisboa").name("Portugal").region("Europa").build(),
            Reservation.builder().id("1").capital("Londres").name("Inglaterra").region("Europa").build(),
            Reservation.builder().id("1").capital("Amsterdã").name("Holanda").region("Europa").build());
}
