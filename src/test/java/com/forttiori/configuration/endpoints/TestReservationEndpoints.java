package com.forttiori.configuration.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forttiori.configuration.controller.ReservationController;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import com.forttiori.configuration.service.reservations.ReservationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ReservationController.class)
@ExtendWith(RestDocumentationExtension.class)
class TestReservationEndpoints {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ReservationServiceImpl reservationService;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    void deveSalvarReservaERetornarIsCreated() throws Exception {
        var json = new ObjectMapper().writeValueAsString(serviceResponseDTO);

        when(reservationService.save(serviceRequestDTO)).thenReturn(serviceResponseDTO);

        this.mockMvc.perform(post("http://localhost:8081/api/paises/reservas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                        .andExpect(status().isCreated())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("id", is("1")))
                        .andExpect(jsonPath("name", is("Brasil")))
                        .andExpect(jsonPath("capital", is("Brasilia")))
                        .andExpect(jsonPath("region", is("America do Sul")))
                        .andDo(print())
                        .andDo(document("{methodName}",
                            preprocessRequest(prettyPrint()),
                            preprocessResponse(prettyPrint())));
    }

    @Test
    void deveRetornarTodasReservasIsOk() throws Exception {
        var json = new ObjectMapper().writeValueAsString(serviceResponseDTO);

        when(reservationService.findAll()).thenReturn(MapperService.mapListReservationToListResponse(reservationList));

        this.mockMvc.perform(get("http://localhost:8081/api/paises/reservas/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));

    }

    @Test
    void deveRetornarReservaByIdIsOk() throws Exception {

        var json = new ObjectMapper().writeValueAsString(serviceResponseDTO);

        when(reservationService.findByIDResponse("1")).thenReturn(serviceResponseDTO);

        this.mockMvc.perform(get("http://localhost:8081/api/paises/reservas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is("1")))
                .andExpect(jsonPath("name", is("Brasil")))
                .andExpect(jsonPath("capital", is("Brasilia")))
                .andExpect(jsonPath("region", is("America do Sul")))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    @Test
    void deveAtualizarReservaIsOk() throws Exception {

        var json = new ObjectMapper().writeValueAsString(serviceResponseDTO);

        when(reservationService.update("1", serviceRequestDTO)).thenReturn(serviceResponseDTO);

        this.mockMvc.perform(put("http://localhost:8081/api/paises/reservas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", is("1")))
                .andExpect(jsonPath("name", is("Brasil")))
                .andExpect(jsonPath("capital", is("Brasilia")))
                .andExpect(jsonPath("region", is("America do Sul")))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    @Test
    void deveDeletarUmaReservaNoContent() throws Exception {

        this.mockMvc.perform(delete("http://localhost:8081/api/paises/reservas/id"))
                .andExpect(status().isNoContent())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    @Test
    void deveDeleteManyReservasIsOk() throws Exception {
        List<String> ids = Arrays.asList("1", "2", "3");

        when(reservationService.deleteMany(ids)).thenReturn(reservationList);

        this.mockMvc.perform(delete("http://localhost:8081/api/paises/reservas?ids=1,2,3"))
                .andExpect(status().isNoContent())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].name", is("Portugal")))
                .andExpect(jsonPath("$[0].capital", is("Lisboa")))
                .andExpect(jsonPath("$[0].region", is("Europa")))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    ServiceRequestDTO serviceRequestDTO = ServiceRequestDTO.builder()
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    ServiceResponseDTO serviceResponseDTO = ServiceResponseDTO.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    List<Reservation> reservationList = Arrays.asList(
            Reservation.builder().id("1").capital("Lisboa").name("Portugal").region("Europa").build(),
            Reservation.builder().id("2").capital("Londres").name("Inglaterra").region("Europa").build(),
            Reservation.builder().id("3").capital("Amsterdã").name("Holanda").region("Europa").build());
}
