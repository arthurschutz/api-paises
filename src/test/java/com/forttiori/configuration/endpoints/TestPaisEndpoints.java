package com.forttiori.configuration.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forttiori.configuration.controller.PaisController;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.service.countries.PaisServiceImpl;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PaisController.class)
@ExtendWith(RestDocumentationExtension.class)
class TestPaisEndpoints {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    PaisServiceImpl paisService;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    void deveRetornarPaisPelaCapitalIsOk() throws Exception {

        paisInfoResponseList.add(paisInfoResponse);
        var json = new ObjectMapper().writeValueAsString(paisInfoResponseList);

        when(paisService.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(paisInfoResponseList);

        this.mockMvc.perform(get("http://localhost:8081/api/paises/capital/Brasilia")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    @Test
    void deveRetornarPaisPeloNomeIsOk() throws Exception {

        paisInfoResponseList.add(paisInfoResponse);
        var json = new ObjectMapper().writeValueAsString(paisInfoResponseList);

        when(paisService.getPaisByName(paisInfoResponse.getName())).thenReturn(paisInfoResponseList);

        this.mockMvc.perform(get("http://localhost:8081/api/paises/name/Brasil")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));

    }

    @Test
    void deveRetornarPaisPelaRegiaoIsOk() throws Exception {

        paisInfoResponseList.add(paisInfoResponse);
        var json = new ObjectMapper().writeValueAsString(paisInfoResponseList);

        when(paisService.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(paisInfoResponseList);

        this.mockMvc.perform(get("http://localhost:8081/api/paises/region/americas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    @Test
    void deveRetornarTodosPaisesIsOk() throws Exception {

        paisInfoResponseList.add(paisInfoResponse);
        var json = new ObjectMapper().writeValueAsString(paisInfoResponseList);

        when(paisService.getTodosPaises()).thenReturn(paisInfoResponseList);

        this.mockMvc.perform(get("http://localhost:8081/api/paises/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andDo(document("{methodName}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())));
    }

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasilia")
            .region("América do Sul")
            .build();

    List<PaisInfoResponse> paisInfoResponseList = new ArrayList<>();
}
