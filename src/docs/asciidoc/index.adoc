:sectnums:
:sectnumlevels: 5
:toc: left
:toclevels: 3
:page-layout: docs

= Documentação API: Utilizando multimódulos Gradle e consumindo uma API
:author: Arthur Schutz
:email: arthur.schutz123@gmail.com
:revnumber: v1.1
:revdate: 09/02/2021

== *Reservation Controller - Endpoints para fazer reservas -*

=== Verbos

|===
|Verbos HTTP|Path|Description

|POST
|"/"
|Salva uma reserva

|GET
|"/reservations"
|Retorna todas as reservas já feitas

|GET
|"/{id}"
|Retorna a reserva pelo id

|PUT
|"/{id}"
|Atualiza a reserva pelo id

|DELETE
|"/{id}"
|Deleta a reserva pelo id

|DELETE
|"...?id=1&id=2"
|Deleta mais de uma reserva pelo id

|===

=== Parâmetros

|===
|Path|Type|Description

|Nome
|String
|Nome do país

|Região
|String
|Região do país

|Capital
|String
|Capital do país

|Id
|String
|ID da reserva

|===

=== Save - POST

_Request_
include::{snippets}/deveSalvarReservaERetornarIsCreated/http-request.adoc[]

_Response_
include::{snippets}/deveSalvarReservaERetornarIsCreated/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveSalvarReservaERetornarIsCreated/curl-request.adoc[]

=== FindAll - GET

_Request_
include::{snippets}/deveRetornarTodasReservasIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarTodasReservasIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarTodasReservasIsOk/curl-request.adoc[]

=== FindByID - GET

_Request_
include::{snippets}/deveRetornarReservaByIdIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarReservaByIdIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarReservaByIdIsOk/curl-request.adoc[]

=== Update - PUT

_Request_
include::{snippets}/deveAtualizarReservaIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveAtualizarReservaIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveAtualizarReservaIsOk/curl-request.adoc[]

=== Deletar Uma Reserva - DELETE

_Request_
include::{snippets}/deveDeletarUmaReservaNoContent/http-request.adoc[]

_Response_
include::{snippets}/deveDeletarUmaReservaNoContent/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveDeletarUmaReservaNoContent/curl-request.adoc[]

=== Deletar Várias Reservas - DELETE

_Request_
include::{snippets}/deveDeleteManyReservasIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveDeleteManyReservasIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveDeleteManyReservasIsOk/curl-request.adoc[]

== *País Controller - Endpoints para consumir a API externa -*

=== Verbos

|===
|Verbos HTTP|Path|Description

|GET
|"/capital/{capital}"
|Retorna o país pela capital

|GET
|"/region/{region}"
|Retorna o país pela região

|GET
|"/name/{name}"
|Retorna o país pelo nome

|GET
|"/todos"
|Retorna todos os países

|===

=== Parâmetros

|===
|Path|Type|Description

|Nome
|String
|Nome do país

|Região
|String
|Região do país

|Capital
|String
|Capital do país

|===

=== GetPaisByCapital - GET

_Request_
include::{snippets}/deveRetornarPaisPelaCapitalIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarPaisPelaCapitalIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarPaisPelaCapitalIsOk/curl-request.adoc[]

=== GetPaisByRegiao - GET

_Request_
include::{snippets}/deveRetornarPaisPelaRegiaoIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarPaisPelaRegiaoIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarPaisPelaRegiaoIsOk/curl-request.adoc[]

=== GetPaisByName - GET

_Request_
include::{snippets}/deveRetornarPaisPeloNomeIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarPaisPeloNomeIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarPaisPeloNomeIsOk/curl-request.adoc[]

=== GetTodosPaises - GET

_Request_
include::{snippets}/deveRetornarTodosPaisesIsOk/http-request.adoc[]

_Response_
include::{snippets}/deveRetornarTodosPaisesIsOk/http-response.adoc[]

_Exemplo URL_
include::{snippets}/deveRetornarTodosPaisesIsOk/curl-request.adoc[]