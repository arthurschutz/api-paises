package com.forttiori.configuration.integration.service;

import com.forttiori.configuration.integration.client.PaisClient;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PaisServiceIntegration {

    private final PaisClient paisClient;

    public List<PaisInfoResponse> getPaisByName(String name) {
        return this.paisClient.getPaisByName(name);
    }

    public List<PaisInfoResponse> getPaisByCapital(String capital) {
        return this.paisClient.getPaisByCapital(capital);
    }

    public List<PaisInfoResponse> getPaisByRegiao(String region) {
        return this.paisClient.getPaisByRegiao(region);
    }

    public List<PaisInfoResponse> getTodosPaises() {
        return this.paisClient.getTodosPaises();
    }

}
