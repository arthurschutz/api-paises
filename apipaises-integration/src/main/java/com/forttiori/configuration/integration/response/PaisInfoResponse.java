package com.forttiori.configuration.integration.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaisInfoResponse {

    @ApiModelProperty(value = "Nome do país")
    private String name;

    @ApiModelProperty(value = "Região do país")
    private String region;

    @ApiModelProperty(value = "Capital do país")
    private String capital;
}
