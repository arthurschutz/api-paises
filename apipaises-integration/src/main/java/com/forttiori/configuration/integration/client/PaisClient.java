package com.forttiori.configuration.integration.client;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Component("PaisClient")
@FeignClient(name = "pais-client", url = "https://restcountries.eu/rest/v2/")
public interface PaisClient {

    @GetMapping("capital/{capital}")
    List<PaisInfoResponse> getPaisByCapital(@PathVariable("capital") String capital);

    @GetMapping("name/{name}")
    List<PaisInfoResponse> getPaisByName(@PathVariable("name") String name);

    @GetMapping("region/{region}")
    List<PaisInfoResponse> getPaisByRegiao(@PathVariable("region") String region);

    @GetMapping
    List<PaisInfoResponse> getTodosPaises();
}
