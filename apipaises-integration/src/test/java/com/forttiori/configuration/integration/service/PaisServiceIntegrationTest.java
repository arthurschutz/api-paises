package com.forttiori.configuration.integration.service;

import com.forttiori.configuration.integration.client.PaisClient;
import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaisServiceIntegrationTest {

    @Mock
    PaisClient paisClient;

    @InjectMocks
    PaisServiceIntegration paisServiceIntegration;

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América")
            .build();

    List<PaisInfoResponse> listaPaises = new ArrayList<>();

    @Test
    void deveRetorarOPaisPeloNome() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByName(paisInfoResponse.getName())).thenReturn(listaPaises);

        var response = paisServiceIntegration.getPaisByName(paisInfoResponse.getName());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetorarOPaisPelaCapital() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(listaPaises);

        var response = paisServiceIntegration.getPaisByCapital(paisInfoResponse.getCapital());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetorarOPaisPelaRegiao() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(listaPaises);

        var response = paisServiceIntegration.getPaisByRegiao(paisInfoResponse.getRegion());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetorarTodosOsPaises() {

        listaPaises.add(paisInfoResponse);

        when(paisClient.getTodosPaises()).thenReturn(listaPaises);

        var response = paisServiceIntegration.getTodosPaises();
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }
}