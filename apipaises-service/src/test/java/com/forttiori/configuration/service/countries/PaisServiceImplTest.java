package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.integration.service.PaisServiceIntegration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PaisServiceImplTest {

    @Mock
    PaisServiceIntegration paisServiceIntegration;

    @InjectMocks
    PaisServiceImpl paisService;

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América")
            .build();

    List<PaisInfoResponse> listaPaises = new ArrayList<>();

    @Test
    void deveRetorarOPaisPelaCapital() {
        listaPaises.add(paisInfoResponse);

        when(paisServiceIntegration.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(listaPaises);

        var response = paisService.getPaisByCapital(paisInfoResponse.getCapital());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarCapitalNotFound() {
        when(paisServiceIntegration.getPaisByCapital(paisInfoResponse.getCapital())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "CAPITAL NÃO ENCONTRADA!"));
        var thrown = assertThrows(ResponseStatusException.class,
                () -> paisServiceIntegration.getPaisByCapital("Brasília"));
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    void deveRetorarOPaisPeloNome() {

        listaPaises.add(paisInfoResponse);

        when(paisServiceIntegration.getPaisByName(paisInfoResponse.getName())).thenReturn(listaPaises);

        var response = paisService.getPaisByName(paisInfoResponse.getName());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarNomeNotFound() {
        when(paisServiceIntegration.getPaisByName(paisInfoResponse.getName())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "PAÍS NÃO ENCONTRADO!"));
        var thrown = assertThrows(ResponseStatusException.class,
                () -> paisServiceIntegration.getPaisByName("Brasil"));
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    void deveRetorarOPaisPelaRegiao() {

        listaPaises.add(paisInfoResponse);

        when(paisServiceIntegration.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(listaPaises);

        var response = paisService.getPaisByRegiao(paisInfoResponse.getRegion());
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }

    @Test
    void deveRetornarRegiaoNotFound() {
        when(paisServiceIntegration.getPaisByRegiao(paisInfoResponse.getRegion())).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "REGIÃO NÃO ENCONTRADA!"));
        var thrown = assertThrows(ResponseStatusException.class,
                () -> paisServiceIntegration.getPaisByRegiao("América"));
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    void deveRetorarTodosOsPaisess() {

        when(paisServiceIntegration.getTodosPaises()).thenReturn(listaPaises);

        var response = paisService.getTodosPaises();
        var stubExpect = listaPaises;

        assertEquals(stubExpect, response);
    }
}