package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.forttiori.configuration.service.dto.MapperService.mapListReservationToListResponse;
import static com.forttiori.configuration.service.dto.MapperService.mapToServiceResponse;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class ReservationServiceImplTest {

    @Mock
    ReservationRepository reservationRepository;

    @InjectMocks
    ReservationServiceImpl reservationService;

    @Test
    void deveSalvarReserva() {
        when(reservationRepository.save(MapperService.mapToReservationEntity(serviceRequestDTO))).thenReturn(reservation);

        var atual = reservationService.save(serviceRequestDTO);
        var stubExpected = serviceResponseDTO;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetornarListaReservas() {

        when(reservationRepository.findAll()).thenReturn(Collections.singletonList(reservation));

        var atual = reservationService.findAll();
        var stubExpected = Stream.of(reservation)
                .map(dto -> ServiceResponseDTO.builder()
                    .id(dto.getId())
                    .name(dto.getName())
                    .capital(dto.getCapital())
                    .region(dto.getRegion())
                    .build())
                .collect(Collectors.toList());

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetornarReservaEntityById() {

        when(reservationRepository.findById("1")).thenReturn(Optional.ofNullable(reservation));

        var atual = reservationService.findByIdEntity("1");
        var stubExpected = reservation;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetornarReservaResponseById() {

        when(reservationRepository.findById("1")).thenReturn(Optional.ofNullable(reservation));

        var atual = reservationService.findByIDResponse(serviceResponseDTO.getId());
        var stubExpected = mapToServiceResponse(reservation);

        assertEquals(stubExpected, atual);

    }

    @Test
    void deveAtualizarReserva() {

        when(reservationRepository.existsById(reservation.getId())).thenReturn(true);
        when(reservationRepository.save(this.reservation)).thenReturn(this.reservation);
        var atual = reservationService.update("1", this.serviceRequestDTO);
        var stubExpected = ServiceResponseDTO.builder()
                .id(reservation.getId())
                .name(reservation.getName())
                .capital(reservation.getCapital())
                .region(reservation.getRegion())
                .build();
        assertEquals(stubExpected, atual);
    }

    @Test
    void deveLancarExceptionNotFound() {
        when(reservationRepository.existsById(this.reservation.getId())).thenReturn(false);
        var thrown = assertThrows(ResponseStatusException.class,
                () -> reservationService.update("1", this.serviceRequestDTO));
        assertEquals(HttpStatus.NOT_FOUND, thrown.getStatus());
    }

    @Test
    void deveDeletarUmaReserva() {
        when(reservationRepository.existsById(reservation.getId())).thenReturn(true);
        reservationService.delete(reservation.getId());
        verify(reservationRepository, times(1)).deleteById(reservation.getId());
    }

    @Test
    void deveDeletarVariasReservas() {
        Iterable<Reservation> reservations = (reservationList);
        Iterable<String> ids = Arrays.asList("1", "2", "3", "4", "5");

        when(reservationRepository.findAllById(ids)).thenReturn(reservations);

        var deletados = this.reservationService.deleteMany((List<String>) ids);
        var esperados = mapListReservationToListResponse((List<Reservation>) reservations);

        assertEquals(esperados, deletados);
        verify(reservationRepository, times(1)).deleteAll(reservations);
    }

    ServiceResponseDTO serviceResponseDTO = ServiceResponseDTO.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasília")
            .region("América do Sul")
            .build();

    Reservation reservation = Reservation.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasília")
            .region("América do Sul")
            .build();

    ServiceRequestDTO serviceRequestDTO = ServiceRequestDTO.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América do Sul")
            .build();

    List<Reservation> reservationList = new ArrayList<>();
}