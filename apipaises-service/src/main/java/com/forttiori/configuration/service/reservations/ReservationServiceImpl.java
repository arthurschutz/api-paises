package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.persistence.repository.ReservationRepository;
import com.forttiori.configuration.service.dto.MapperService;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static com.forttiori.configuration.service.dto.MapperService.mapListReservationToListResponse;
import static com.forttiori.configuration.service.dto.MapperService.mapToServiceResponse;

@Service
@AllArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    @Override
    public ServiceResponseDTO save(ServiceRequestDTO serviceRequestDTO) {
        return Optional.ofNullable(
                MapperService.mapToServiceResponse(
                        reservationRepository.save(MapperService.mapToReservationEntity(serviceRequestDTO))))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Servidor com problema"));
    }
    @Override
    public List<ServiceResponseDTO> findAll() {
        return mapListReservationToListResponse(
                this.reservationRepository.findAll());
    }

    @Override
    public Reservation findByIdEntity (String id) {
        Optional<Reservation> reservation = this.reservationRepository.findById(id);
        return reservation.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva nao encontrada"));
    }

    @Override
    public ServiceResponseDTO findByIDResponse(String id) {
        return mapToServiceResponse(this.findByIdEntity(id));
    }

    @Override
    public ServiceResponseDTO update(String id, ServiceRequestDTO serviceRequestDTO) {
        if (!reservationRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva não encontrada");
        }
        return Optional.ofNullable(
                MapperService.mapToServiceResponse(reservationRepository.save(
                        MapperService.toUserUpdate.apply(id, serviceRequestDTO))))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Servidor com problema"));
    }

    @Override
    public void delete(String id) {
        if(!reservationRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Reserva não encontrada");
        }
        reservationRepository.deleteById(id);
    }

    @Override
    public List<Reservation> deleteMany(List<String> ids){
        Optional<Iterable<Reservation>> reservations = Optional.of(this.reservationRepository.findAllById(ids));
        reservations.ifPresent(this.reservationRepository::deleteAll);
        return (List<Reservation>) reservations.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Reservas não encontradas"));
    }

}
