package com.forttiori.configuration.service.dto;

import com.forttiori.configuration.entity.Reservation;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Builder
public class MapperService {

    private MapperService(){
    }

    public static ServiceResponseDTO mapToServiceResponse(Reservation reservation) {
        return Optional.ofNullable(reservation)
                .map(reservationEntity -> ServiceResponseDTO.builder()
                    .name(reservation.getName())
                    .capital(reservation.getCapital())
                    .region(reservation.getRegion())
                    .id(reservation.getId())
                    .build())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Dados inválidos"));
    }

    public static Reservation mapToReservationEntity(ServiceRequestDTO serviceRequestDTO) {
        return Optional.ofNullable(serviceRequestDTO)
                .map(reservationDTO -> Reservation.builder()
                    .name(reservationDTO.getName())
                    .capital(reservationDTO.getCapital())
                    .region(reservationDTO.getRegion())
                    .build())
                .orElseThrow(IllegalArgumentException::new);
    }

    public static List<ServiceResponseDTO> mapListReservationToListResponse(List<Reservation> reservations){
        return reservations.stream()
                .map(MapperService::mapToServiceResponse)
                .collect(Collectors.toList());
    }

    public static final BiFunction<String, ServiceRequestDTO, Reservation> toUserUpdate =
            (id, request) -> Optional.ofNullable(Reservation.builder()
                    .id(id)
                    .name(request.getName())
                    .region(request.getRegion())
                    .capital(request.getCapital())
                    .build())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,"Dados inválidos"));
}
