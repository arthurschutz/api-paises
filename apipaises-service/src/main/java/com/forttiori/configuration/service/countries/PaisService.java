package com.forttiori.configuration.service.countries;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PaisService {

    List<PaisInfoResponse> getPaisByCapital(String capital);
    List<PaisInfoResponse> getPaisByName(String name);
    List<PaisInfoResponse> getPaisByRegiao(String region);
    List<PaisInfoResponse> getTodosPaises();
}
