package com.forttiori.configuration.service.reservations;

import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ReservationService {

    ServiceResponseDTO save(ServiceRequestDTO serviceRequestDTO);
    List<ServiceResponseDTO> findAll();
    Reservation findByIdEntity (String id);
    ServiceResponseDTO findByIDResponse(String id);
    ServiceResponseDTO update(String id, ServiceRequestDTO serviceRequestDTO);
    void delete(String id);
    List<Reservation> deleteMany(List<String> ids);
}
