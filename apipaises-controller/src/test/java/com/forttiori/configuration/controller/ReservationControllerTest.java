package com.forttiori.configuration.controller;

import com.forttiori.configuration.controller.dto.ControllerRequestDTO;
import com.forttiori.configuration.controller.dto.ControllerResponseDTO;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import com.forttiori.configuration.service.reservations.ReservationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReservationControllerTest {

    @Mock
    ReservationService reservationService;

    @InjectMocks
    ReservationController reservationController;

    @Test
    void deveSalvarReserva() {
        when(reservationService.save(serviceRequestDTO)).thenReturn(serviceResponseDTO);

        var atual = this.reservationController.save(controllerRequestDTO);
        var stubExpected = ControllerResponseDTO.builder()
                .id(serviceResponseDTO.getId())
                .name(serviceResponseDTO.getName())
                .capital(serviceResponseDTO.getCapital())
                .region(serviceResponseDTO.getRegion())
                .build();

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveFindAllReservas() {
        when(reservationService.findAll()).thenReturn(Collections.singletonList(serviceResponseDTO));

        var atual = reservationController.findAll();
        var stubExpected = Stream.of(controllerResponseDTO)
                .map(dto -> ControllerResponseDTO.builder()
                    .id(dto.getId())
                    .name(dto.getName())
                    .capital(dto.getCapital())
                    .region(dto.getRegion())
                    .build())
                .collect(Collectors.toList());

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveFindReservaByID() {
        when(reservationService.findByIDResponse("1")).thenReturn(serviceResponseDTO);

        var atual = reservationController.findByID("1");
        var stubExpected = controllerResponseDTO;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveAtualizarReserva() {
        when(reservationService.update("1", serviceRequestDTO)).thenReturn(serviceResponseDTO);

        var atual = reservationController.update("1", controllerRequestDTO);
        var stubExpected = ControllerResponseDTO.builder()
                .id(controllerResponseDTO.getId())
                .name(controllerResponseDTO.getName())
                .region(controllerResponseDTO.getRegion())
                .capital(controllerResponseDTO.getCapital())
                .build();

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveDeletarUmaReserva() {
        reservationService.delete("1");
        verify(reservationService, times(1)).delete("1");
    }

    @Test
    void deveDeleteManyReservas() {
        List<Reservation> reservas = reservationList;
        List<String> ids = Arrays.asList("1", "2", "3");

        when(reservationService.deleteMany(ids)).thenReturn(reservas);

        var atual = reservationController.deleteMany(ids);
        var stubExpected = reservas;

        assertEquals(stubExpected, atual);
    }

    ServiceRequestDTO serviceRequestDTO = ServiceRequestDTO.builder()
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    ServiceResponseDTO serviceResponseDTO = ServiceResponseDTO.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    ControllerRequestDTO controllerRequestDTO = ControllerRequestDTO.builder()
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    ControllerResponseDTO controllerResponseDTO = ControllerResponseDTO.builder()
            .id("1")
            .name("Brasil")
            .capital("Brasilia")
            .region("America do Sul")
            .build();

    List<Reservation> reservationList = Arrays.asList(
            Reservation.builder().id("1").capital("Lisboa").name("Portugal").region("Europa").build(),
            Reservation.builder().id("1").capital("Londres").name("Inglaterra").region("Europa").build(),
            Reservation.builder().id("1").capital("Amsterdã").name("Holanda").region("Europa").build());
}