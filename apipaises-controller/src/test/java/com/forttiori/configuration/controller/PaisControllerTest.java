package com.forttiori.configuration.controller;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.service.countries.PaisServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaisControllerTest {

    @Mock
    PaisServiceImpl paisService;

    @InjectMocks
    PaisController paisController;

    @Test
    void deveRetorarOPaisPelaCapital() {
        when(paisService.getPaisByCapital(paisInfoResponse.getCapital())).thenReturn(paisInfoResponseList);

        var atual = paisController.getPaisByCapital(paisInfoResponse.getCapital());
        var stubExpected = paisInfoResponseList;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarOPaisPeloNome() {
        when(paisService.getPaisByName(paisInfoResponse.getName())).thenReturn(paisInfoResponseList);

        var atual = paisController.getPaisByName(paisInfoResponse.getName());
        var stubExpected = paisInfoResponseList;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarOPaisPelaRegiao() {
        when(paisService.getPaisByRegiao(paisInfoResponse.getRegion())).thenReturn(paisInfoResponseList);

        var atual = paisController.getPaisByRegiao(paisInfoResponse.getRegion());
        var stubExpected = paisInfoResponseList;

        assertEquals(stubExpected, atual);
    }

    @Test
    void deveRetorarTodosOsPaises() {
        when(paisService.getTodosPaises()).thenReturn(paisInfoResponseList);

        var atual = paisController.getTodosPaises();
        var stubExpected = paisInfoResponseList;

        assertEquals(stubExpected, atual);
    }

    PaisInfoResponse paisInfoResponse = PaisInfoResponse.builder()
            .name("Brasil")
            .capital("Brasília")
            .region("América do Sul")
            .build();

    List<PaisInfoResponse> paisInfoResponseList = new ArrayList<>();
}