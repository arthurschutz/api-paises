package com.forttiori.configuration.controller;

import com.forttiori.configuration.controller.dto.ControllerRequestDTO;
import com.forttiori.configuration.controller.dto.ControllerResponseDTO;
import com.forttiori.configuration.controller.dto.MapperController;
import com.forttiori.configuration.entity.Reservation;
import com.forttiori.configuration.service.reservations.ReservationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("api/paises/reservas")
@AllArgsConstructor
@Api("Reservation Resource")
public class ReservationController {

    private final ReservationService reservationService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponse(code = 201, message = "Reserva cadastrada")
    @ApiOperation("Salva uma reserva no banco de dados")
    public ControllerResponseDTO save(@RequestBody @Valid ControllerRequestDTO controllerRequestDTO) {
        return MapperController.mapToControllerResponse(reservationService.save(MapperController.mapToServiceRequest(controllerRequestDTO)));
    }

    @GetMapping("/reservations")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(code = 200, message = "Lista com todas as reservas")
    @ApiOperation("Retorna todas as reservas já feitas")
    public List<ControllerResponseDTO> findAll() {
        return this.reservationService.findAll().stream()
                .map(MapperController::mapToControllerResponse)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a reserva pelo ID"),
            @ApiResponse(code = 404, message = "Reserva não encontrada")
    })
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Retorna a reserva pelo ID")
    public ControllerResponseDTO findByID(@PathVariable String id) {
        return MapperController.mapToControllerResponse(reservationService.findByIDResponse(id));
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(code = 200, message = "Reserva atualizada")
    @ApiOperation("Atualiza a reserva")
    public ControllerResponseDTO update(@PathVariable String id, @RequestBody @Valid ControllerRequestDTO controllerRequestDTO) {
        return MapperController.mapToControllerResponse(
                reservationService.update(id, MapperController.mapToServiceRequest(controllerRequestDTO)));
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponse(code = 204, message = "Reserva deletada pelo ID")
    @ApiOperation("Deleta uma reserva")
    public void delete(@PathVariable String id) {
        this.reservationService.delete(id);
    }

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    @ApiResponse(code = 204, message = "Reservas deletadas pelos ID's")
    @ApiOperation("Reservas deletadas pelos ID's")
    public List<Reservation> deleteMany(@RequestParam("ids") List<String> ids) {
        return reservationService.deleteMany(ids);
    }
}
