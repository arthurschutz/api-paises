package com.forttiori.configuration.controller;

import com.forttiori.configuration.integration.response.PaisInfoResponse;
import com.forttiori.configuration.service.countries.PaisServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/paises")
@AllArgsConstructor
@Api("Countries Resource")
public class PaisController {

    private final PaisServiceImpl paisService;

    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o país pela capital"),
            @ApiResponse(code = 404, message = "Capital não encontrada"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção")
    })
    @ApiOperation("Retorna as infos do país a partir da capital")
    @GetMapping("/capital/{capital}")
    public List<PaisInfoResponse> getPaisByCapital(@PathVariable String capital) {
         return this.paisService.getPaisByCapital(capital);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o país pelo nome"),
            @ApiResponse(code = 404, message = "País não encontrado"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção")
    })
    @ApiOperation("Retorna as infos do país a partir do nome")
    @GetMapping("/name/{name}")
    public List<PaisInfoResponse> getPaisByName(@PathVariable String name) {
        return this.paisService.getPaisByName(name);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna os paises pela região"),
            @ApiResponse(code = 404, message = "Região não encontrada"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção")
    })
    @ApiOperation("Retorna as infos do país a partir da região")
    @GetMapping("/region/{region}")
    public List<PaisInfoResponse> getPaisByRegiao(@PathVariable String region) {
        return this.paisService.getPaisByRegiao(region);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiResponse(code = 200, message = "Retorna todos os países")
    @ApiOperation("Retorna as infos de todos os país")
    @GetMapping("/todos")
    public List<PaisInfoResponse> getTodosPaises() {
        return this.paisService.getTodosPaises();
    }
}
