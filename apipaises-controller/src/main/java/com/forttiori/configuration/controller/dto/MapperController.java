package com.forttiori.configuration.controller.dto;

import com.forttiori.configuration.service.dto.ServiceRequestDTO;
import com.forttiori.configuration.service.dto.ServiceResponseDTO;
import lombok.Builder;

import java.util.Optional;

@Builder
public class MapperController {

    private MapperController(){
    }

    public static ControllerResponseDTO mapToControllerResponse(ServiceResponseDTO serviceResponseDTO) {
        return Optional.ofNullable(serviceResponseDTO)
                .map(pessoaContrato -> ControllerResponseDTO.builder()
                    .name(pessoaContrato.getName())
                    .capital(pessoaContrato.getCapital())
                    .region(pessoaContrato.getRegion())
                    .id(pessoaContrato.getId())
                    .build())
                .orElseThrow(IllegalArgumentException::new);
    }

    public static ServiceRequestDTO mapToServiceRequest(ControllerRequestDTO controllerRequestDTO) {
        return Optional.ofNullable(controllerRequestDTO)
                .map(serviceRequest -> ServiceRequestDTO.builder()
                    .name(serviceRequest.getName())
                    .capital(serviceRequest.getCapital())
                    .region(serviceRequest.getRegion())
                    .build())
                .orElseThrow(IllegalArgumentException::new);
    }
}
